package ru.tsc.karbainova.tm.service.model;

import lombok.NonNull;
import ru.tsc.karbainova.tm.api.service.IConnectionService;
import ru.tsc.karbainova.tm.model.AbstractOwnerEntity;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity> extends AbstractService<E> {

    @NonNull
    public AbstractOwnerService(@NonNull final IConnectionService connectionService) {
        super(connectionService);
    }
}
