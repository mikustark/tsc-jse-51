package ru.tsc.karbainova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.model.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {
    void clear();

    void clearByUserId(@NotNull String userId);

    Project findByName(@Nullable String userId, @Nullable String name);

    List<Project> findAll();

    List<Project> findAllByUserId(@Nullable String userId);

    Project findByIdUserId(@Nullable String userId, @Nullable String id);

    @Nullable Project findByIndex(@Nullable String userId, @NotNull Integer index);

    void remove(@NotNull Project entity);

    void removeById(@Nullable String id);

    void removeByName(@Nullable String userId, @Nullable String name);

    void removeByIdAndUserId(@Nullable String userId, @NotNull String id);

    void removeByIndex(@NotNull String userId, int index);

    int getCount();

    int getCountByUser(@NotNull String userId);
}
