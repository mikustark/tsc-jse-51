package ru.tsc.karbainova.tm.api.service.model;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.model.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskServiceModel {
    @SneakyThrows
    List<Task> findAll();

    @SneakyThrows
    List<Task> findAllTaskByUserId(String userId);

    @SneakyThrows
    void addAll(Collection<Task> collection);

    @SneakyThrows
    void clear();

    @SneakyThrows
    Task add(Task task);

    @SneakyThrows
    void create(@NonNull String userId, @NonNull String name, @NonNull String description);

    @SneakyThrows
    void remove(@NonNull String userId, @NonNull Task task);

    @SneakyThrows
    Task updateById(@NonNull String userId, @NonNull String id,
                    @NonNull String name, @Nullable String description);

    @SneakyThrows
    Task findByName(@NonNull String userId, @NonNull String name);

    @SneakyThrows
    Task findByIdUserId(@NonNull String userId, @NonNull String id);
}
