package ru.tsc.karbainova.tm.api.repository.dto;

import ru.tsc.karbainova.tm.dto.AbstractDTOEntity;

public interface IDTORepository<E extends AbstractDTOEntity> {
    void add(E entity);

    void update(E entity);
}
